@extends('blog::layouts.master')

@section('body-title')
    Module: {!! config('blog.name') !!}
@endsection

@section('content')
    <h1>Hello World</h1>

    <p>
        This view is loaded from the module '{!! config('blog.name') !!}'
    </p>


        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Author</th>
                <th scope="col">Title</th>
                <th scope="col">Content</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
            <tr>
                <th scope="row">{{ $post->id }}</th>
                <td>{{ $post->author }}</td>
                <td>{{ $post->title }}</td>
                <td>{{ substr($post->content, 0, 20) }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>

@endsection
